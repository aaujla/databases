local dbConnect = require 'db.Connect'

function main(Data) 

   -- The code uses a single database connection that is managed automatically
   -- by the db.Connect module - the connection automatically created if it 
   -- does not exist and kept open between queries (for improved performance).  
   -- If the database connection is lost for any reason then it automatically
   -- reconnects - the reconnection is retried s specified number of times, 
   -- with a specified interval between the retries. If the reconnection is 
   -- not succesful then the channel is stopped. All connections, retries 
   -- and associated errors are logged.
   
   -- The number of retries (RETRY_COUNT) and pause between retries (RETRY_PAUSE)
   -- and other settings are specified in the Connect.lua module (near the top)
   
   -- Connect.lua uses one database connection, and connects to one database.
   -- For multiple database connections, you can use multiple local copies of
   -- Connect.lua modules and customize the settings as needed.
   
   -- To run the code you need to do two things:
   -- 1) Create a SQL database - we used "test"
   -- 2) Update the DB credentials to match your DB, user and password

   print("@@Start Oracle msgID: "..Data)
   
   -- select all rows from patient_test table
   local result = dbConnect.DbQuery('SELECT * FROM patient_test')
   
   print("@@END Oraccle msgID: "..Data.." "..result[1].FirstName)
end
