-- $Revision: 1.0 $
-- $Date: 2017-10-19 $

--
-- The Database Connection module
-- Copyright (c) 2011-2017 iNTERFACEWARE Inc. ALL RIGHTS RESERVED
-- iNTERFACEWARE permits you to use, modify, and distribute this file in accordance
-- with the terms of the iNTERFACEWARE license agreement accompanying the software
-- in which it is used.

local retry = require "retry"

local Connect = {}
local Conn = false

-- Database credentials
local DBAPI      = db.ORACLE_OCI  -- DB connection type
local DBNAME     = 'XE'    -- name of database you are using
local DBUSER     = 'SYSTEM'    -- name of user to connect to DB
local DBPASSWORD = 'Zxcvbnm123'  -- password of that user

-- Configuration settings
local DBLIVE     = true        -- true = Connect database, false = No Connect database
local DB_EXECUTE  = true       -- true = Update database, false = No Update database 
local RETRY_COUNT = 10          -- number of times to try reconnecting to database
local RETRY_PAUSE = 60          -- number of seconds between trying to reconnect to database
local DB_CONNECT_TIMEOUT = 10  -- number of seconds before the DB connection times out

-- Calculate Database Timeout
local dbTimeout = RETRY_COUNT*(RETRY_PAUSE + DB_CONNECT_TIMEOUT)
if dbTimeout < 300 then timeout = 300 end -- if less than 300 set back to 5m (300 sec) default

-- Local functions -- START --
local function DbConnect() 
   Conn = db.connect{api=DBAPI, name=DBNAME, user=DBUSER, password=DBPASSWORD,live=DBLIVE}
end

-- Initial connect to database
local function Init()
   print("current "..tostring(Conn))
   if not Conn or not Conn:check() then
      print("need retry "..tostring(Conn))
      if Conn then Conn:close() end -- close stale connection
      print("reset conn "..tostring(Conn))
      iguana.setTimeout(dbTimeout) 
      retry.call{func=DbConnect, 
         retry=RETRY_COUNT, pause=RETRY_PAUSE, 
         funcname='Init'}
   end
end

-- Local functions -- END --

-- Execute database
function Connect.DbExecute(Sql)   
   Init()
   return Conn:execute{sql=Sql, live=DB_EXECUTE} 
end

-- Query database
function Connect.DbQuery(Sql)
   Init()
   return Conn:query{sql=Sql, live=true}
end

function Connect.DbMerge(Data, Live, Bulk_insert, Transaction)
   Init()
   Conn:merge{data=Data, bulk_insert=Bulk_insert, transaction=Transaction, live=Live}
end

function Connect.DbQuote(String)
   Init()
   return Conn:quote(String)
end

-- Help for functions
local HelpInfo = [[{
      "SummaryLine": "Executes an ad hoc SQL statement that can alter the database.",
      "Returns": [
         {
            "Desc": "<b>For queries:</b> the first result set <u>result_set node tree</u>."
         },
         {
            "Desc": "<b>For queries:</b> An array containing all the result sets <u>table</u>."
         }
      ],
      "Parameters": [         
         {
            "sql": {
               "Desc": "a string containing the SQL statement <u>string</u>."
            }
         },
         {
            "live": {
               "Desc": " if true, the statement will be executed in the editor (default = false) <u>boolean</u>.",
               "Opt": true
            }
         },
      ],
      "Title": "DbExecute",
      "Usage": "DbConnect.DbExecute(sql)",
      "Examples": [
         "local dbConnect = require 'db.Connect'<br><br>dbConnect.DbExecute('INSERT INTO Patient (FirstName, LastName) VALUES(\"Fred\", \"Smith\")')",
         "local dbConnect = require 'db.Connect'<br><br>-- Return an array containing multiple results sets, one for each query<br>-- Note: Some DBs do not support multiple queries in a single \"sql\" string<br><br>local Result = dbConnectDbExecute('SELECT * FROM Patient; SELECT * FROM Kin')",
         "local dbConnect = require 'db.Connect'<br><br>-- trap the database error thrown by <code>DbConnect.DbExecute</code><br><br>-- try to insert a duplicate key = \"Primary Key Violation\" error<br>local TryDuplicateKey = \"INSERT INTO [dbo].[Patient] (Id,FirstName, LastName) VALUES (1, 'Fred', 'Smith')\"<br><br>local Success, Error = pcall(DbConnect.DbExecute, TryDuplicateKey)<br><br>trace(Success)                 -- false in this case<br>trace(Error)                   -- view the pcall Error return as a table<br>local DbError = Error.message  -- copy the DB error message string from Error<br>trace(DbError)                 -- view the DB error message"
      ],
      "ParameterTable": false,
      "SeeAlso": [
         {
            "Title": "Tools Repository: Database Connection",
            "Link": "http://help.interfaceware.com/v6/database-connection"
         },
         {
            "Title": "Our Database Documentation",
            "Link": "http://help.interfaceware.com/category/building-interfaces/interfaces/database"
         },
         {
            "Title": "The db_connection module - new database methods",
            "Link": "http://wiki.interfaceware.com/1034.html?v=6.0.0"
         },
         {
            "Title": "The db module - old style database functions",
            "Link": "http://wiki.interfaceware.com/431.html?v=6.0.0"
         }
      ],
      "Desc": "Executes an ad hoc SQL statement that can alter the database.<br><br>DbExecute() is designed to enhance the behaviour of the builtin conn:execute{} function. It automatically reconnects when a database connection is lost - both the number of retries (RETRY_COUNT) and the retry delay (RETRY_PAUSE) can be configured in the \"Configuration Settings\" near the top of the Connect.lua module. It also improves performance by using a persistent database connection (that stays open as long as the channel is running).<br><br>Both <code>dbConnect.DbExecute()</code> and <code>dbConnect.DbQuery()</code> throw errors if a database error is encountered. The errors are regular Lua errors and will cause the execution of the script to halt, unless they are caught by Lua error handling using <code>pcall</code> or <code>xpcall</code>. The error thrown is a table with two fields:<ul><li><b>message</b>: a string with the description of the error</li><li><b>code</b>: an integer error code returned by the database.</li></ul>Currently, error codes from ODBC sources and MySQL databases are supported."
   }
]]

help.set{input_function=Connect.DbExecute, help_data=json.parse{data=HelpInfo}}

local HelpInfo = [[{
      "SummaryLine": "Executes an ad hoc SQL query against a database.",
      "Returns": [
         {
            "Desc": "The first result set <u>result_set node tree</u>."
         },
         {
            "Desc": "An array containing all the result sets <u>table</u>."
         }
      ],
      "Parameters": [
         {
            "sql": {
               "Desc": "a string containing the SQL select statement\n"
            }
         },
         {
            "live": {
               "Desc": " if true, the statement will be executed in the editor (default = true) <u>boolean</u>.",
               "Opt": true
            }
         },
      ],
      "Title": "DbQuery",
      "Usage": "DbConnect.DbQuery(sql)",
      "Examples": [
         "local dbConnect = require 'db.Connect'<br><br>local Result = dbConnect.DbQuery('SELECT * FROM Patient WHERE Flag = \"F\"')",
         "local dbConnect = require 'db.Connect'<br><br>-- Return an array containing multiple results sets, one for each query<br>-- Note: Some DBs do not support multiple queries in a single \"sql\" string<br><br>local Result = dbConnect.DbQuery('SELECT * FROM Patient; SELECT * FROM Kin')",
         "local dbConnect = require 'db.Connect'<br><br>-- trap the database error thrown by <code>DbConnect.DbQuery</code><br><br>local Success, Error = pcall(DbConnect.DbQuery, 'Select * from Not_a_table')<br><br>trace(Success)                 -- false in this case<br>trace(Error)                   -- view the pcall Error return as a table<br>local DbError = Error.message  -- copy the DB error message string from Error<br>trace(DbError)                 -- view the DB error message"
      ],
      "ParameterTable": false,
      "SeeAlso": [
         {
            "Title": "Tools Repository: Database Connection",
            "Link": "http://help.interfaceware.com/v6/database-connection"
         },
         {
            "Title": "Our Database Documentation",
            "Link": "http://help.interfaceware.com/category/building-interfaces/interfaces/database"
         },
         {
            "Title": "The db_connection module - new database methods",
            "Link": "http://wiki.interfaceware.com/1034.html?v=6.0.0"
         },
         {
            "Title": "The db module - old style database functions",
            "Link": "http://wiki.interfaceware.com/431.html?v=6.0.0"
         }
      ],
      "Desc": "Executes an ad hoc SQL query against a database.\nInsert or update statements are not allowed (for these, use the <code>conn:execute</code> method).<br><br>DbQuery() is designed to enhance the behaviour of the builtin conn:query{} function. It automatically reconnects when a database connection is lost - both the number of retries (RETRY_COUNT) and the retry delay (RETRY_PAUSE) can be configured in the \"Configuration Settings\" near the top of the Connect.lua module. It also improves performance by using a persistent database connection (that stays open as long as the channel is running).<br><br>Both <code> conn:query()</code> and <code> conn:execute()</code> throw errors if a database error is encountered. The errors are regular Lua errors and will cause the execution of the script to halt, unless they are caught by Lua error handling using <code>pcall</code> or <code>xpcall</code>. The error thrown is a table with two fields:<ul><li><b>message</b>: a string with the description of the error</li><li><b>code</b>: an integer error code returned by the database.</li></ul>Currently, error codes from ODBC sources and MySQL databases are supported."
   }
]]

help.set{input_function=Connect.DbQuery, help_data=json.parse{data=HelpInfo}}

local HelpInfo = [[{
      "Examples": [
         "<pre>local dbConnect = require 'db.Connect'\n\ndbConnect.DbMmerge(My_Data)\n\ndbConnect.DbMmerge(My_Data, _, true, false)</pre>"
      ],
      "Usage": "DbCconnect.DbMerge (Data [, Live] [, Bulk_insert] [, Transaction])",
      "Title": "DbMerge",
      "SummaryLine": "Merges table records into the database.",
      "Desc": "Merges the database tables created by db.tables() into the database.<br><br>DbMerge() is designed to enhance the behaviour of the builtin conn:merge{} function. It automatically reconnects when a database connection is lost - both the number of retries (RETRY_COUNT) and the retry delay (RETRY_PAUSE) can be configured in the \"Configuration Settings\" near the top of the Connect.lua module. It also improves performance by using a persistent database connection (that stays open as long as the channel is running).",
      "ParameterTable": false,
      "SeeAlso": [
         {
            "Title": "Tools Repository: Database Connection",
            "Link": "http://help.interfaceware.com/v6/database-connection"
         },
         {
            "Title": "Our Database Documentation",
            "Link": "http://help.interfaceware.com/category/building-interfaces/interfaces/database"
         },
         {
            "Title": "The db_connection module - new database methods",
            "Link": "http://wiki.interfaceware.com/1034.html?v=6.0.0"
         },
         {
            "Title": "The db module - old style database functions",
            "Link": "http://wiki.interfaceware.com/431.html?v=6.0.0"
         }
      ],
      "Returns": [
      ],
      "Parameters": [
         {
            "data": {
               "Desc": "set to a node table tree created using db.tables <u>node tree</u>."
            }
         },
         {
            "live": {
               "Desc": "if true, the merge will be executed in the editor (default = false) <u>boolean</u>.",
               "Opt": true
            }
         },
         {
            "bulk_insert": {
               "Desc": "set to true to use bulk insert logic (default = false) <u>boolean</u>.",
               "Opt": true
            }
         },
         {
            "transaction": {
               "Desc": "set to false to disable inserting/updating all rows as a transaction (default = true) <u>boolean</u>.",
               "Opt": true
            }
         },
      ]
   }
]]

help.set{input_function=Connect.DbMerge, help_data=json.parse{data=HelpInfo}}

local HelpInfo = [[{
      "Examples": [
         "<pre>local Sql = 'SELECT * FROM MyTable WHERE Name = '..conn:quote(MyString)</pre>"
      ],
      "Usage": "DbCconnect.DbQuote(data)",
      "Title": "DbQuote",
      "SummaryLine": "Returns an escaped string surrounded by single quotes.",
      "Desc": "Accepts a single string argument, and returns an escaped string surrounded by single quotes. Escaping is database specific, characters are escaped specifically to match each database API.",      
      "SeeAlso": [
         {
            "Title": "Tools Repository: Database Connection",
            "Link": "http://help.interfaceware.com/v6/database-connection"
         },
         {
            "Title": "Our Database Documentation",
            "Link": "http://help.interfaceware.com/category/building-interfaces/interfaces/database"
         },
         {
            "Title": "The db_connection module - new database methods",
            "Link": "http://wiki.interfaceware.com/1034.html?v=6.0.0"
         },
         {
            "Title": "The db module - old style database functions",
            "Link": "http://wiki.interfaceware.com/431.html?v=6.0.0"
         }
      ],
      "Returns": [
         {
            "Desc": "An escaped string surrounded by single quotes <u>string</u>."
         }
      ],
      "Parameters": [
         {
            "data": {
               "Desc": "The string to escape <u>string</u>."
            }
         }
      ]
   }
]]
	

help.set{input_function=Connect.DbQuote, help_data=json.parse{data=HelpInfo}}

return Connect